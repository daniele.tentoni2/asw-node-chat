const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http); // Observ events on server.

app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/index.html`);
});

io.on('connection', socket => {
    console.log("User connection");
    socket.on('user-conn', message => {
        console.log(`User ${message} connected`);
        io.emit('user-conn', message);
    });
    socket.on('user-disc', message => {
        console.log(`User ${message} disconnected`);
        io.emit('user-disc', message);
    });
    socket.on('chat message', message => {
        console.log(`Received: ${message}`);
        io.emit('chat message', message);
    });
    socket.on('user-writing', socket => { // Re-emit user-writing events.
        console.log(`Received writing: ${socket}`);
        io.emit('user-writing', socket);
    });
    socket.on('user-waiting', socket => { // Re-emit user-waiting events.
        console.log(`Received waiting: ${socket}`);
        io.emit('user-waiting', socket);
    });
    socket.on('disconnect', socket => { // transport close.
        console.log("Socket disconnected. User left?");
    });
});

const port = process.env.PORT || 8080;

http.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
});